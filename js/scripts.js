$(function() {
  // popovers fix - make data attributes have higher priority than options
  var _getOptionsOriginal = $.fn.popover.Constructor.prototype.getOptions;
  $.fn.popover.Constructor.prototype.getOptions = function (options) {
    options = $.extend({}, options, this.$element.data())
    return _getOptionsOriginal.call(this, options);
  }

  window.collapseCollapsableSection = function(trigger, section, transitionTime){
    if(transitionTime == null)
      transitionTime = 200;
    trigger.find('.open').hide();
    trigger.find('.closed').show();
    trigger.removeClass('expanded-header');
    section.slideUp( transitionTime, function(){
      section.data('collapsable-state', 'collapsed');
      trigger.data('collapsable-transitioning',false)
    });
  }

  window.uncollapseCollapsableSection = function(trigger, section, transitionTime){
    if(transitionTime == null)
      transitionTime = 200;
    trigger.find('.open').show();
    trigger.find('.closed').hide();
    trigger.addClass('expanded-header')
    section.slideDown( transitionTime, function(){
      section.data('collapsable-state', 'expanded');
      trigger.data('collapsable-transitioning',false);
    });
  }

  // (?) popovers
  $('body').popover({
    container: 'body',
    selector: '.help-pop-over',
    template:'<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title-container"><span class="help-pop-over">?</span><span class="popover-title"><span></h3><div class="popover-content"></div><a class="close" data-dismiss="popover"><img src="images/popover-x.png"></img></a></div>'
  }).on('shown.bs.popover', function(e){
    var trigger = $(e.target);
    $("[rel=popover]").not(trigger).popover("destroy");
    // popovers have close button
    $('[data-dismiss="popover"]').off().click(function(){
      $("[rel=popover]").popover("destroy");
    });
  });

  // swipe to change homepage carousel on mobile
  $("#myCarousel").swipe( {
    allowPageScroll: "vertical",
    fallbackToMouseEvents: false,
    swipe:function(event, direction, distance, duration, fingerCount, fingerData) {
      if(direction == "left"){
        $("#myCarousel").carousel("next");
      } else if(direction == "right"){
          $("#myCarousel").carousel("prev");
      }
    }
  });

  // initialize homebrew accordians
  $('[data-collapsable]').each(function(){
    if($(this).data('collapsable-state')){
    }
    else{
      $(this).data('collapsable-state', 'collapsed')
    }

    trigger = $("[data-collapsable-target='"+$(this).attr('id')+"']");
    if(trigger.data('collapsableClickAction') == null || trigger.data('collapsableClickAction') === ""){
      trigger.data('collapsableClickAction', 'toggle');
    }

    if($(this).data('collapsable-state') == 'collapsed'){
      collapseCollapsableSection(trigger, $(this), 0);
    }else{
      uncollapseCollapsableSection(trigger, $(this), 0);
    }
  });

  // homebrew accordian user interaction
  $(document).on('click', '[data-collapsable-target]', function(e){
    e.preventDefault(true);

    var clicked = $(this);
    if(clicked.data('collapsableClickAction') != "toggle")
      return;

    var collapsableTarget = $("#"+clicked.data('collapsable-target'));
    var theMenuIcon = clicked.find('.menu-icon');

    if(clicked.data('collapsable-transitioning') == true){
      return;
    }

    clicked.data('collapsable-transitioning',true);

    // if not expanded -> expand
    if(collapsableTarget.data('collapsable-state') == 'collapsed'){
      uncollapseCollapsableSection(clicked, collapsableTarget);
    }else{
      collapseCollapsableSection(clicked, collapsableTarget);
    }
  });
});
// merge files - replicate file selectors
$(function(){
  var minFileSelectTemplateInstances = 1;
  var fileSelectTemplate = $('.file-select-template');
  var customizeFileSelectTemplateInstance = function(instance, instanceIdNum){
    var name = instance.children('label').attr('for') + '-' + instanceIdNum.toString();
    instance.children('label').attr('for', name);
    instance.find('select').attr('id', name);
    instance.find('select').attr('name', name);
  }
  var addFileSelectTemplateInstance = function(){
    var instances = $('.file-select-template-instance').not('.file-select-template .file-select-template-instance');
    var after = instances.length > 0 ? instances.last() : fileSelectTemplate;
    var copy = $(fileSelectTemplate.html());
    customizeFileSelectTemplateInstance(copy, instances.length);
    after.after(copy);
  }
  // generate initial copies
  for(var i = 0; i < minFileSelectTemplateInstances; i++)
    addFileSelectTemplateInstance();
  // generate more copies when all instances are filled
  $(document).on('change', '.file-select-template-instance select', function(e){
    var instances = $('.file-select-template-instance').not('.file-select-template .file-select-template-instance');
    var createAnotherCopy = true;
    for(var i = 0; i < instances.length; i++){
      if($(instances[i]).find('select').val() == ""){
        createAnotherCopy = false;
        break;
      }
    }
    if(createAnotherCopy)
      addFileSelectTemplateInstance();
  });
});
