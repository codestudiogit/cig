$(function(){
	var reportWiz = $('.report-create-wiz');
	if(reportWiz.length == 0)
		return;

	var stepsContainer = $('.report-create-wiz-steps');
	var summaryContainer = $('.report-create-wiz-summary');

	var steps = [];
	var stepElems = $('.report-create-collapsable-section');
	var currentStep = 0;
	var summaryStep = stepElems.length;

	reportWiz.find('.next-btn').click(function(){
		gotoNextStep();
	})
	reportWiz.find('.prev-btn').click(function(){
		gotoPrevStep();
	})

	stepElems.each(function(){
		steps.push({
			trigger: $(this).find('.collapsable-section-header'),
			collapsable: $(this).find('[data-collapsable]')
		});
	})

	$.each(steps, function(){
		collapseStep(this, 0);
	});
	uncollapseStep(steps[0], 0);
	gotoSteps();

	function collapseStep(step, duration){
		collapseCollapsableSection(step.trigger, step.collapsable, duration)
	}

	function uncollapseStep(step, duration){
		uncollapseCollapsableSection(step.trigger, step.collapsable, duration)
	}

	function gotoNextStep(){
		steps[currentStep].trigger.addClass("complete");
		if(currentStep == steps.length - 1){
			gotoSummary();
			currentStep++;
		}else{
			collapseStep(steps[currentStep]);
			currentStep++;
			uncollapseStep(steps[currentStep]);
		}
	}

	function gotoPrevStep(){
		if(currentStep == 0)
			return;
		if(currentStep == summaryStep){
			gotoSteps();
			currentStep--;
		}else{
			collapseStep(steps[currentStep]);
			currentStep--;
			uncollapseStep(steps[currentStep]);
		}
	}

	function gotoSummary(){
		stepsContainer.hide();
		summaryContainer.show();
	}

	function gotoSteps(){
		summaryContainer.hide();
		stepsContainer.show();		
	}
});